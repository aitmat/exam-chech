<?php

namespace App\DataFixtures;

use App\Entity\FavoritePost;
use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewUser([
            'email' => '123@123.ru',
            'passport' => 'some & passport',
            'password' => 'pass123',
        ]);
        $manager->persist($user);

        $user2 = $this->userHandler->createNewUser([
            'email' => '111@111.ru',
            'passport' => 'AN11111',
            'password' => 'pass111',
        ]);
        $manager->persist($user2);

        $user3 = $this->userHandler->createNewUser([
            'email' => 'qwe@qwe.qwe',
            'passport' => 'qwe',
            'password' => 'qwe',
        ]);
        $manager->persist($user3);

        $post1 = new FavoritePost();
        $post1
            ->setUser($user)
            ->setIdPost('t3_8wgone')
            ->setAddedAt(new \DateTime('2018-05-07 10:32:10'))
        ;
        $manager->persist($post1);

        $post2 = new FavoritePost();
        $post2
            ->setUser($user)
            ->setIdPost('t3_7e2x3e')
            ->setAddedAt(new \DateTime('2017-01-07 10:32:10'));
        $manager->persist($post2);

        $post3 = new FavoritePost();
        $post3
            ->setUser($user)
            ->setIdPost('t3_8loxz1')
            ->setAddedAt(new \DateTime('2018-11-07 10:32:10'));
        $manager->persist($post3);

        $post4 = new FavoritePost();
        $post4
            ->setUser($user)
            ->setIdPost('t3_80607r')
            ->setAddedAt(new \DateTime('2018-12-07 10:32:10'));
        $manager->persist($post4);

        $post5 = new FavoritePost();
        $post5
            ->setUser($user)
            ->setIdPost('t3_8cgj3g')
            ->setAddedAt(new \DateTime('2018-01-07 10:32:10'));
        $manager->persist($post5);

        $post5 = new FavoritePost();
        $post5
            ->setUser($user)
            ->setIdPost('t3_8ouvwv')
            ->setAddedAt(new \DateTime('2018-08-07 10:32:10'));
        $manager->persist($post5);

        $post7 = new FavoritePost();
        $post7
            ->setUser($user)
            ->setIdPost('t3_8g1n0z')
            ->setAddedAt(new \DateTime('2018-07-07 10:32:10'));
        $manager->persist($post7);

        $post8 = new FavoritePost();
        $post8
            ->setUser($user)
            ->setIdPost('t3_8qa3p9')
            ->setAddedAt(new \DateTime('2018-05-07 10:32:10'));
        $manager->persist($post8);

        $post9 = new FavoritePost();
        $post9
            ->setUser($user)
            ->setIdPost('t3_8qy0td')
            ->setAddedAt(new \DateTime('2018-04-07 10:32:10'));
        $manager->persist($post9);

        $post10 = new FavoritePost();
        $post10
            ->setUser($user)
            ->setIdPost('t3_8qy0td')
            ->setAddedAt(new \DateTime('2018-05-01 10:32:10'));
        $manager->persist($post10);

        $post11 = new FavoritePost();
        $post11
            ->setUser($user2)
            ->setIdPost('t3_8wgone')
            ->setAddedAt(new \DateTime('2018-05-09 10:32:10'))
        ;
        $manager->persist($post1);

        $post12 = new FavoritePost();
        $post12
            ->setUser($user2)
            ->setIdPost('t3_7e2x3e')
            ->setAddedAt(new \DateTime('2017-01-08 10:32:10'));
        $manager->persist($post12);

        $post13 = new FavoritePost();
        $post13
            ->setUser($user2)
            ->setIdPost('t3_8loxz1')
            ->setAddedAt(new \DateTime('2018-11-06 10:32:10'));
        $manager->persist($post13);

        $post14 = new FavoritePost();
        $post14
            ->setUser($user2)
            ->setIdPost('t3_80607r')
            ->setAddedAt(new \DateTime('2018-12-05 10:32:10'));
        $manager->persist($post14);

        $post15 = new FavoritePost();
        $post15
            ->setUser($user2)
            ->setIdPost('t3_8cgj3g')
            ->setAddedAt(new \DateTime('2018-01-04 10:32:10'));
        $manager->persist($post15);

        $post16 = new FavoritePost();
        $post16
            ->setUser($user2)
            ->setIdPost('t3_8ouvwv')
            ->setAddedAt(new \DateTime('2018-08-03 10:32:10'));
        $manager->persist($post16);

        $post17 = new FavoritePost();
        $post17
            ->setUser($user2)
            ->setIdPost('t3_8g1n0z')
            ->setAddedAt(new \DateTime('2018-07-03 10:32:10'));
        $manager->persist($post17);

        $post18 = new FavoritePost();
        $post18
            ->setUser($user3)
            ->setIdPost('t3_8qa3p9')
            ->setAddedAt(new \DateTime('2018-05-02 10:32:10'));
        $manager->persist($post18);

        $post20 = new FavoritePost();
        $post20
            ->setUser($user3)
            ->setIdPost('t3_8qy0td')
            ->setAddedAt(new \DateTime('2018-04-02 10:32:10'));
        $manager->persist($post20);

        $post21 = new FavoritePost();
        $post21
            ->setUser($user3)
            ->setIdPost('t3_8qy0td')
            ->setAddedAt(new \DateTime('2018-05-01 10:32:10'));
        $manager->persist($post21);

        $post22 = new FavoritePost();
        $post22
            ->setUser($user3)
            ->setIdPost('t3_8qy0td')
            ->setAddedAt(new \DateTime('2018-05-01 10:32:10'));
        $manager->persist($post22);

        $post23 = new FavoritePost();
        $post23
            ->setUser($post23)
            ->setIdPost('t3_8qy0td')
            ->setAddedAt(new \DateTime('2018-05-01 10:32:10'));
        $manager->persist($post21);

        $post24 = new FavoritePost();
        $post24
            ->setUser($user3)
            ->setIdPost('t3_8qy0td')
            ->setAddedAt(new \DateTime('2018-05-01 10:32:10'));
        $manager->persist($post24);

        $post25 = new FavoritePost();
        $post25
            ->setUser($user3)
            ->setIdPost('t3_8qy0td')
            ->setAddedAt(new \DateTime('2018-05-01 10:32:10'));
        $manager->persist($post25);

        $manager->flush();
    }
}
