<?php

namespace App\Repository;

use App\Entity\FavoritePost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FavoritePost|null find($id, $lockMode = null, $lockVersion = null)
 * @method FavoritePost|null findOneBy(array $criteria, array $orderBy = null)
 * @method FavoritePost[]    findAll()
 * @method FavoritePost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoritePostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FavoritePost::class);
    }

    /**
     * @param $user
     * @return mixed|null
     */
    public function getMyFavoritePosts($user)
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @param $user
     * @return mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkAlreadyAddedMyFavorite($id, $user)
    {
        try {
            return $this->createQueryBuilder('u')
                ->select('u')
                ->where('u.user = :user')
                ->andWhere('u.id_post = :id')
                ->setParameter('user', $user)
                ->setParameter('id', $id)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function countFavoriteAdded($id)
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.id_post = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function getFavoritePosts()
    {
        return $this->createQueryBuilder('a')
            ->select('a.id_post, COUNT(a.id_post) AS HIDDEN b')
            ->groupBy('a.id_post')
            ->orderBy('b', "desc")
            ->getQuery()
            ->getResult();
    }
}
