<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class indexController extends Controller
{
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(Request $request, ObjectManager $manager, UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $data = [
                'email' => $user->getEmail(),
                'passport' => $user->getPassport(),
                'password' => $user->getPassword(),
            ];
            $user = $userHandler->createNewUser($data);
            $manager->persist($user);
            $manager->flush();
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute("homepage");
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/sign-in", name="sign-in")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signInAction(UserRepository $userRepository, Request $request, UserHandler $userHandler)
    {
        $form = $this->createForm("App\Form\AuthenticationType");
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $user = $userRepository->findByEmailAndPassword($data['email'], $userHandler->encodePlainPassword($data['password']));
            if ($user){
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute("homepage");
            }
        }
        return $this->render('sign_in.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
