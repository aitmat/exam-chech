<?php

namespace App\Model\User;

use App\Entity\User;

use App\Model\Api\ApiContext;
use Psr\Container\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var ApiContext
     */
    private $apiContext;

    public function __construct(ContainerInterface $container, ApiContext $apiContext)
    {
        $this->container = $container;
        $this->apiContext = $apiContext;
    }

    /**
     * @param array $data
     * @return User
     */
    public function createNewUser(array $data) {
        $user = new User();
        $user->setEmail($data['email']);
        $user->setPassport($data['passport']);
        $user->setPassword($this->encodePlainPassword($data['password']));
        return $user;
    }

    /**
     * @param string $password
     * @return string
     */
    public function encodePlainPassword(string $password): string
    {
        return  md5($password) . md5($password . '2');
    }


    public function makeUserSession(User $user) {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }
}
